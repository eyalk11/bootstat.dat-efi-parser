# BOOTSTAT.DAT EFI parser

Parser for the EFI BOOTSTAT.DAT file in Windows 10. The file is normally located in EFI/Microsoft/Boot/BOOTSTAT.DAT

## Usage

```
./bootstat.dat-efi-parser.py <outputtype> <BOOTSTAT.DAT-file>
```

where outputtype is either `json` or `csv`. The json output is correct json, while the csv output is a bastardized format with header information first and odd header + event-line following.

On Windows 7, the C:\\Windows\\bootstat.dat file can be parsed by cutting away the first 2048 bytes, which seems to be an extra header.

```
dd if=bootstat.dat of=bootstat.dat.cut bs=1 skip=2048
```

## Example output

```javascript
{
  "version": 4,
  "header_size": 24,
  "file_size": 65536,
  "valid_data_size": 208,
  "unknown_header_dword_0": 24,
  "unknown_header_dword_1": 0,
  "events": [
    {
      "event_name": "Log file initialised",
      "timestamp": 6176,
      "zero_field": 0,
      "source_guid": "2C86EA9DDD5C704EACC1F32B344D4795",
      "size_of_entry": 64,
      "severity_code": 1,
      "entry_version": 2,
      "event_identifier": 1,
      "event_time_struct": "2018-01-01 12:00:00",
      "event_zero_field_0": 0,
      "event_seven": 7,
      "event_one": 1,
      "event_zero_field_1": 0
    },
    {
      "event_name": "Boot application launch",
      "timestamp": 6177,
      "zero_field": 0,
      "source_guid": "2C86EA9DDD5C704EACC1F32B344D4795",
      "size_of_entry": 120,
      "severity_code": 1,
      "entry_version": 2,
      "event_identifier": 17,
      "event_app_guid": "80A054721015854EAC0FE7FB3D444736",
      "event_type_of_start": 0,
      "event_app_pathname": "\\windows\\system32\\winload.efi"
    }
  ]
}
```

## Dependencies

The program works with a vanilla python 3.6 install.

## Links

Main inspiration found at Geoff Chappell's site which specifies the BOOTSTAT.DAT format for Windows Vista:

http://www.geoffchappell.com/notes/windows/boot/bsd.htm
